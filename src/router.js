//pages 
import Home from './components/home';
import Instruments from './components/instruments';
import Favorites from './components/favorities';
import Nav from './components/nav';

import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink
} from "react-router-dom";

export default (
  <Router>
      <div className="grid-x">
        <Nav />

        <div class="cell medium-10 content">
          <Switch>
            <Route path="/instruments">
              <Instruments />
            </Route>
            <Route path="/favorites">
              <Favorites />
            </Route>
            <Route path="/"  component={Home} />
          </Switch>
        </div>
      </div>
    </Router>
);
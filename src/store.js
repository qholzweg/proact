import { createStore } from 'redux';
import _ from 'lodash';
import { loadState, saveState } from './sessionStorage';

const initialState = {
  instruments: [], 
  favs: [],
  current_page:1,
  sort: 'works_count', 
  sort_direction: 'desc', 
}
const checkFavs = function(state, cms){
  if (state.favs && state.favs.length > 0) {
    for (var fav of state.favs) {
      var index = cms.findIndex(a => a.id === fav.id);
      if (~index) Object.assign(cms[index], {fav:true}); 
    }
  }
  return cms;
}

const rootReducer = function(state = initialState, action) {
  var newFavs;
  switch (action.type) {
    case 'ADD_FAV' : 
      newFavs = state.favs.concat([action.cms]);
      return Object.assign({}, state, { favs: newFavs });
    
    case 'REMOVE_FAV' :
      newFavs = _.filter(state.favs, favs => favs.id !== action.cmsId);
      return Object.assign({}, state, { favs: newFavs });

    case 'GET_CMS' : 
    var checkedCms = checkFavs(state, action.cms);
    var newState = Object.assign({}, state, { instruments: checkedCms, current_page:action.current_page});
    if (action.sort) Object.assign(newState, {sort:action.sort});
    if (action.sort_direction) Object.assign(newState, {sort_direction:action.sort_direction});
    return newState;
  }
  
  return state;
}

const persistedState = loadState();
const store = createStore(rootReducer, persistedState);
store.subscribe(() => {
  saveState(store.getState());
});

export default store;

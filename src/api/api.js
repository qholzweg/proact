import axios from 'axios';

/**
 * Get cms
 */

export function getCms(page = 1, sort='works_count', sort_direction='desc') {
  return axios.get('https://api.cmsmagazine.ru/v1/instrumentsList', {
    params: {
      instrument_type_code: 'cms',
      page: page,
      sort: sort,
      sort_direction: sort_direction,
    }
  })
    .then(response => response.data);
}


import React, { Component } from 'react';

class FavoritiesList extends Component {
  render() {
    return (
      <table className="instrument-list">
        <tbody>
        {this.props.favs.map(function(cms) {
          return (
            <tr key={cms.id}>
              <td>
              <a className="instrument-logo" href={'/instrument/' + cms.code}>{cms.image ? (<img alt={cms.title} src={cms.image} />) : (<h2 className="simbol-logo ">{cms.firstLettersOfName}</h2>) }</a>
                <span className='title-block'>
                  <span className='title'>{cms.title}</span>
                  {cms.isSponsor ? (<a className='shortUrl' href={cms.url}>{cms.shortUrl}</a>) : ''}
                </span>
              </td>
              <td>
                {cms.worksCount} проектов
              </td>
              <td>
                {cms.partnersCount} партнера
              </td>
              <td>
                {cms.rate}
              </td>
              <td>
                <a className="button hollow alert rounded" href="#" onClick={(e) => this.props.deleteFav(cms.id, e)}>Удалить</a>
              </td>
            </tr>
          );
        }.bind(this))}
        </tbody>
      </table>
    );
  }
}

export default FavoritiesList;
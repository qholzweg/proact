import React, { Component } from 'react';
import logo from '../logo.png';
import {
  Link,
  NavLink
} from "react-router-dom";
import { connect } from 'react-redux';

class Nav extends Component {
  render() {
   return (
    <nav className="cell medium-2 sidebar">
      <ul className="menu vertical main-menu">
        <li className="logo">
          <img src={logo} alt="Logo" />
        </li>
        <li>
          <Link to="/">Журнал</Link>
        </li>
        <li>
          <Link to="/">Агентства</Link>
        </li>
        <li>
          <NavLink activeClassName='is-active' to="/instruments">Инструменты</NavLink>
        </li>
      </ul>
      <ul className="menu vertical aux-menu">
        <li>
        <NavLink activeClassName='is-active' to="/favorites" className="favs">Избранное</NavLink>
        {this.props.favs.length > 0 ? (<span class="badge">{this.props.favs.length}</span>) : 0}
        </li>
        <li>
          <Link to="/" className="search">Поиск</Link>
        </li>
        <li>
          <Link to="/" className="lk">Кабинет агентства</Link>
        </li>
      </ul>
    </nav>
    );
  }
}

const mapStateToProps = function(store) {
  return {
    favs: store.favs
  };
};

export default connect(mapStateToProps)(Nav);
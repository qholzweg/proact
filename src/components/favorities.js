import React, { Component } from 'react';
import FavoritiesList from './favoritiesList';
import { connect } from 'react-redux';
import store from '../store';

class Favorites extends Component {
  constructor(props) {
    super(props);

    //bind this to functions
    this.deleteFav = this.deleteFav.bind(this);
  }

  deleteFav(cmsId, e) {
    store.dispatch({
      type: 'REMOVE_FAV',
      cmsId: cmsId
    });
  }
  render() {
    return (
    <div className="favorities">
      <h3>Выбранные компании:</h3>
      {this.props.favs.length > 0 ? 
      (<FavoritiesList favs={this.props.favs} deleteFav={this.deleteFav} />) :
      (<div className="callout">Пока пусто</div>)
      }
    </div>
    );
  };
}
const mapStateToProps = function(store) {
  return {
    favs: store.favs
  };
};

export default connect(mapStateToProps)(Favorites);
import React, { Component } from 'react';

class InstrumentsList extends Component {
  render() {
    return (
    <div className="instruments-list">
      <table className="instrument-list">
        <thead>
          <tr>
            <th className="sponsor"></th>
            <th className="cms">
              Название
            </th>
            <th className="projects">
              <a href="#" onClick={(e) => this.props.onSort('works_count', e)} className={this.props.sort === 'works_count' ? 'active ' + this.props.sort_direction : ''}>Проекты </a>
            </th>
            <th className="partners">
            <a href="#" onClick={(e) => this.props.onSort('partners_count', e)} className={this.props.sort === 'partners_count' ? 'active ' + this.props.sort_direction : ''}>Партнеры</a>
            </th>
            <th className="rate">
            <a href="#" onClick={(e) => this.props.onSort('rate', e)} className={this.props.sort === 'rate' ? 'active ' + this.props.sort_direction : ''}>Оценка пользователя</a>
            </th>
            <th className="fav">
              Сравнить
            </th>
          </tr>
        </thead>
        <tbody>
        {this.props.instruments.map(function(instrument) {
          // instrument.isSponsor = true;
          return (
            <tr key={instrument.id} className={instrument.isSponsor ? 'isSponsor' : ''}>
              {instrument.isSponsor ? (<td><img src="https://cmsmagazine.ru/images/main_menu/star_red.svg" /></td>) : (<td></td>)}
              <td>
                <a className="instrument-logo" href={'/instrument/' + instrument.code}>{instrument.image ? (<img alt={instrument.title} src={instrument.image} />) : (<h2 className="simbol-logo ">{instrument.firstLettersOfName}</h2>) }</a>
                <span className='title-block'>
                  <span className='title'>{instrument.title}</span>
                  {instrument.isSponsor ? (<a className='shortUrl' href={instrument.url}>{instrument.shortUrl}</a>) : ''}
                </span>
              </td>
              <td>
                {instrument.worksCount} проектов
              </td>
              <td>
                {instrument.partnersCount} партнера
              </td>
              <td>
                {instrument.rate}
              </td>
              <td>
                <label class="container">
                  <input type="checkbox" defaultChecked={instrument.fav ? 'checked' : ''} onChange={(e) => this.props.onFavChange(instrument, e)} />
                  <span class="checkmark"></span>
                </label>
              </td>
            </tr>
          );
        }.bind(this))}
        </tbody>
      </table>
      <nav aria-label="Pagination">
      <ul className="pagination">
        <li className={this.props.current_page === 1 ? 'disabled' : 'pagination-previous'}><a onClick={(e) => this.props.changePage('prev', e)} href="#" aria-label="Previous page"> </a></li>
        <li className="current"><span className="show-for-sr">You're on page</span> {this.props.current_page}</li>
        <li className="pagination-next"><a onClick={(e) => this.props.changePage('next', e)} href="#" aria-label="Next page"></a></li>
        </ul>
      </nav>
    </div>
    );
  }
}

export default InstrumentsList;
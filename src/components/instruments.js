import React, { Component } from 'react';
import { connect } from 'react-redux';
import store from '../store';
import InstrumentsList from './instrumentsList';
import * as api from '../api/api';

class Instruments extends Component {
  constructor(props) {
    super(props);

    //bind this to functions
    this.onSort = this.onSort.bind(this);
    this.changePage = this.changePage.bind(this);
    this.onFavChange = this.onFavChange.bind(this);
    this.getToggledDirection = this.getToggledDirection.bind(this);
  }
  componentDidMount() {
    var current_page = this.props.current_page ? this.props.current_page : 1;
    var sort = this.props.sort ? this.props.sort : 'work_counts';
    var direction = this.props.sort_direction ? this.props.sort_direction : 'desc';
    api.getCms(current_page, sort, direction).then(data => {
      store.dispatch({
        type: 'GET_CMS',
        cms: data.data,
        current_page:data.current_page,
        sort: sort, 
        sort_direction: direction
      });
    });
  }
  getToggledDirection() {
    if (this.props.sort_direction === 'desc') {
      return 'asc';
    } else {
      return 'desc';
    }
  }
  onSort(sort, e) {
    e.preventDefault();
    var direction = 'desc';
    if (this.props.sort === sort) {
      direction = this.getToggledDirection();
    }
    api.getCms(this.props.current_page, sort, direction).then(data => {
      store.dispatch({
        type: 'GET_CMS',
        cms: data.data,
        current_page:data.current_page,
        sort: sort, 
        sort_direction: direction
      });
    });
  }
  changePage(direction, e) {
    e.preventDefault();
    var page;
    if (direction === 'next') {
      page = this.props.current_page + 1;
    } else if (direction === 'prev') {
      page = this.props.current_page - 1;
    }
    api.getCms(page, this.props.sort, this.props.sort_direction).then(data => {
      store.dispatch({
        type: 'GET_CMS',
        cms: data.data,
        current_page:data.current_page,
      });
    });
  }
  onFavChange(cms, e) {
    if (e.target.checked) {
      store.dispatch({
        type: 'ADD_FAV',
        cms: cms
      });
    } else {
      store.dispatch({
        type: 'REMOVE_FAV',
        cmsId: cms.id
      });
    }
  }

  render() {
    return <InstrumentsList 
      current_page={this.props.current_page} 
      sort={this.props.sort} 
      sort_direction={this.props.sort_direction} 
      instruments={this.props.instruments} 
      onSort={this.onSort} 
      changePage={this.changePage} 
      onFavChange={this.onFavChange} />
  }

}
const mapStateToProps = function(store) {
  return {
    favs: store.favs,
    instruments:store.instruments,
    current_page:store.current_page,
    sort: store.sort, 
    sort_direction: store.sort_direction
  };
};

export default connect(mapStateToProps)(Instruments);
import React from 'react';
import './scss/App.scss';
import {Provider} from 'react-redux';
import store from './store';
import router from './router';


function App() {
  return (
    // router
    <Provider store={store}>{router}</Provider>
  );
}

export default App;
